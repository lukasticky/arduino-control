# Arduino Control

Start specific functions on Arduino by sending predefined commands over serial via the click of a button. This tool is made as a simple user interface for hardware where routines can be controlled over serial input.

This code should work on all platforms, but has only been tested on Linux (Fedora 32 Workstation) & Windows (Windows 10 Build 19041.450) with Python 3.8.3.

## Development

Install dependencies: 

```sh
pip3 install --user -r requirements.txt
```

Run:

```sh
python3 acontrol.py
```

### Customization

Adjust the following lines/blocks when modifying the buttons:

* `acontrol.py:60`
* `acontrol.py:76`
* `acontrol.py:97`
* `acontrol.py:114`

After modifying `window.ui` you have to regenerate the window module `window.py` like this:

```sh
pyuic5 window.ui -o window.py
```

### Bundle

Add DLLs to Path on Windows:

```bat
set PATH=%PATH%;C:\Windows\System32\downlevel;
```

Build executable:

```sh
pyinstaller -F acontrol.spec
```
